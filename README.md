# vue-final

## Tentang Aplikasi
Sebuah website restoran yang menampilkan menu makanan yg dijual dan bisa melakukan order melalui web.

## Teknologi 
1. Vue CLI
2. SCSS
3. PWA
4. Axios
5. Firebase
<!-- 
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
``` -->

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
